@extends('layouts/main')
@section('footer')
    <footer class="page-footer text-center font-small mt-4 wow fadeIn">
        <div class="container">
            <small class="copyright">Designed <i class="fa fa-heart"></i> by <a href="http://themes.3rdwavemedia.com/" target="_blank">At1Xr1s</a> for MALOY</small>
        </div>
    </footer>
@endsection

@extends('layouts/main')
@section('header')
    <header id="header" class="header">
        <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
            <div align="right" class="container">
                <a href="#" class="navbar=brand waves-effect">
                    <strong class="black-text">At1</strong>
                </a>
                <button class="navbar-toogler " type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent"
                        aria-expanded="false" aria-label="Toogle Navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a href="#" class="nav-link waves-effect">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="goto" href="/calc">Calc</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
@endsection

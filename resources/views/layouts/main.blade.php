<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>At1Xr1s - Какойто сайт с табличками</title>
    <!-- Bootstrap css connection -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">



</head>
<body>
@section('header')
    <header id="header" class="header">
        <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
            <div align="right" class="container">
                <a href="#" class="navbar=brand waves-effect">
                    <strong class="black-text">At1</strong>
                </a>
                <button class="navbar-toogler " type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent"
                        aria-expanded="false" aria-label="Toogle Navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a href="/" class="nav-link waves-effect">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link waves-effect" href="/calc">Calc</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
@endsection
@yield('header')




@section('table')
    <div align="right" class="container">
        <button class="navbar-toogler " type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent"
                aria-expanded="false" aria-label="Toogle Navigation">
            <span class="navbar-toggler-icon"></span>
            <li class="nav-item active">
                <a href="/" class="nav-link waves-effect">Какая-то там блять кнопка</a>
            </li>
        </button>
    </div>
    <table border="2">
        <caption>Таблица размеров обуви</caption>
        <tr>
            <th>Россия</th>
            <th>Великобритания</th>
            <th>Европа</th>
            <th>Длина ступни, см</th>
        </tr>
        <tr><td>34,5</td><td>3,5</td><td>36</td><td>23</td></tr>
        <tr><td>35,5</td><td>4</td><td>36⅔</td><td>23–23,5</td></tr>
        <tr><td>36</td><td>4,5</td><td>37⅓</td><td>23,5</td></tr>
        <tr><td>36,5</td><td>5</td><td>38</td><td>24</td></tr>
        <tr><td>37</td><td>5,5</td><td>38⅔</td><td>24,5</td></tr>
        <tr><td>38</td><td>6</td><td>39⅓</td><td>25</td></tr>
        <tr><td>38,5</td><td>6,5</td><td>40</td><td>25,5</td></tr>
        <tr><td>39</td><td>7</td><td>40⅔</td><td>25,5–26</td></tr>
        <tr><td>40</td><td>7,5</td><td>41⅓</td><td>26</td></tr>
        <tr><td>40,5</td><td>8</td><td>42</td><td>26,5</td></tr>
        <tr><td>41</td><td>8,5</td><td>42⅔</td><td>27</td></tr>
        <tr><td>42</td><td>9</td><td>43⅓</td><td>27,5</td></tr>
        <tr><td>43</td><td>9,5</td><td>44</td><td>28</td></tr>
        <tr><td>43,5</td><td>10</td><td>44⅔</td><td>28–28,5</td></tr>
        <tr><td>44</td><td>10,5</td><td>45⅓</td><td>28,5–29</td></tr>
        <tr><td>44,5</td><td>11</td><td>46</td><td>29</td></tr>
        <tr><td>45</td><td>11,5</td><td>46⅔</td><td>29,5</td></tr>
        <tr><td>46</td><td>12</td><td>47⅓</td><td>30</td></tr>
        <tr><td>46,5</td><td>12,5</td><td>48</td><td>30,5</td></tr>
        <tr><td>47</td><td>13</td><td>48⅔</td><td>31</td></tr>
        <tr><td>48</td><td>13,5</td><td>49⅓</td><td>31,5</td></tr>
    </table>
@endsection
@yield('table')





@section('footer')
    <footer class="page-footer text-center font-small mt-4 wow fadeIn">
        <div class="container">
            <small class="copyright">Designed <i class="fa fa-heart"></i> by <a href="http://themes.3rdwavemedia.com/" target="_blank">At1Xr1s</a> for MALOY in 2019</small>
        </div>
    </footer>
@endsection
@yield('footer')
<!-- Boostrap js connection -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

</body>
</html>
